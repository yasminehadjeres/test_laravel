@extends('layouts.app')

@section('content')
<div class="container">
    
    @if (Auth::check())
        @forelse($tickets as $ticket)
        <div class="card mt-4">
            <div class="card-header">Ticket {{$ticket->id}}</div>
            <div class="card-body">
                <!-- <a href="/task" class="btn btn-primary">Add new Ticket</a> -->
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <!-- <th scope="col">Select</th> -->
                        <th scope="col">Name</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Email</th>
                        <th scope="col">Job title</th>
                        <th scope="col">Company</th>
                        <th scope="col">Country</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($ticket->contacts as $contact)
                    <tr>
                        <!-- <td>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck{{$contact->id}}" >
                            <label class="custom-control-label" for="customCheck{{$contact->id}}">{{ $contact->id}}</label>
                        </div>
                        </td> -->
                        <td>{{ $contact->name}}</td>
                        <td>{{ $contact->lastname}}</td>
                        <td>{{ $contact->email}}</td>
                        <td>{{ $contact->title}}</td>
                        <td>{{ $contact->company}}</td>
                        <td>{{ $contact->country}}</td>
                    </tr>
                @endforeach
                
                </tbody>
                </table>
                <div class="card-footer">
                    <form action="/ticket/{{$ticket->id}}/edit">
                    <!-- <a class="btn btn-small btn-info" href="{{ route('tickets.edit', [$ticket]) }}">IZAAANE</a> -->

                    <a class="btn btn-small btn-info" href="{{ URL::route('tickets.edit', $ticket->id) }}">Solve Ticket</a>
                        
                        
                        <!-- {{ csrf_field() }} -->
                    </form>
                </div>
            </div>
            
        </div> 
        @empty
        <div class="card mt-4">
            There are no tickets.
        </div>
        @endforelse

    @else
        <div class="card mt-4">
            <div class="card-body">
                <h3>You need to log in. <a href="/login">Click here to login</a></h3>
            </div>
        </div>
    
    @endif
               
</div>
@endsection
