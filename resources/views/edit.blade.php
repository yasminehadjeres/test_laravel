@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card mt-4">
        <div class="card-header">Ticket {{$ticket->id}}</div>
            <div class="card-body">
                <!-- <a href="/task" class="btn btn-primary">Add new Ticket</a> -->
                <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Select</th>
                        <th scope="col">Name</th>
                        <th scope="col">Lastname</th>
                        <th scope="col">Email</th>
                        <th scope="col">Job title</th>
                        <th scope="col">Company</th>
                        <th scope="col">Country</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($ticket->contacts as $contact)
                    <tr>
                        <td>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck{{$contact->id}}" >
                            <label class="custom-control-label" for="customCheck{{$contact->id}}">{{ $contact->id}}</label>
                        </div>
                        </td>
                        <td>{{ $contact->name}}</td>
                        <td>{{ $contact->lastname}}</td>
                        <td>{{ $contact->email}}</td>
                        <td>{{ $contact->title}}</td>
                        <td>{{ $contact->company}}</td>
                        <td>{{ $contact->country}}</td>
                    </tr>
                @endforeach  
                </tbody>
                </table>
            </div>    
        </div> 

    
    <h2>Correct contact :</h2>
    <form method="POST" action="{{ route('tickets.update', $ticket->id) }}">
        {{ csrf_field() }}

        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Name</label>
                <input type="text" class="form-control" id="inputEmail4" name="name">
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Lastname</label>
                <input type="text" class="form-control" id="inputPassword4" name="lastname">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputAddress">Email</label>
                <input type="email" class="form-control" id="inputAddress" name="email">
            </div>
            <div class="form-group col-md-6">
                <label for="inputAddress2">Job title</label>
                <input type="text" class="form-control" id="inputAddress2" name="title">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputCity">Company</label>
                <input type="text" class="form-control" id="inputCity" name="company">
            </div>
            <div class="form-group col-md-6">
                <label for="inputCountry">Country</label>
                <input type="text" class="form-control" id="inputCountry" name="country">
            
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Correct</button>
        <!-- <a class="btn btn-small btn-info" type="submit" href="{{ URL::route('tickets') }}">Correct contact</a> -->
    </form>
</div>
@stop