<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    public $timestamps = false;

    public function tickets()
    {
        return $this->belongsToMany('App\Ticket', 'contact_ticket', 'ticket_id', 'contact_id');
    }
}
