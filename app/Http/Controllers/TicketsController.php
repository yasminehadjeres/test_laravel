<?php

namespace App\Http\Controllers;

use App\Contact;
use App\ModifiedContact;
use App\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class TicketsController extends Controller
{
    //
    public function index()
    {
        $user = Auth::user();
        $tickets = Ticket::where([['is_active', '=', false], ['waiting_for_validation', '=', false], ['done', '=', false]])->get();
        return view('home', compact('user', 'tickets'));
    }
    
    public function edit($id)
    {
        $user = Auth::user();
        $ticket = Ticket::find($id);
        $ticket->is_active = true;
        $ticket->user_id = $user->id;
        $ticket->save();
        return view('edit', compact('ticket'));
    }

    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $ticket = Ticket::find($id);
        $modified_contact = new ModifiedContact();

        $modified_contact->name = request("name");
        $modified_contact->lastname = request("lastname");
        $modified_contact->email = request("email");
        $modified_contact->title = request("title");
        $modified_contact->company = request("company");
        $modified_contact->country = request("country");
        $modified_contact->is_processed = true;
        $modified_contact->ticket_id = $id;
        $modified_contact->save();

        $ticket->validation_date = Carbon::now();
        $ticket->waiting_for_validation = true;
        $ticket->save();

        $tickets = Ticket::where([['is_active', '=', false], ['waiting_for_validation', '=', false], ['done', '=', false]])->get();
        return view('home', compact('user', 'tickets'));
        
    }
}
