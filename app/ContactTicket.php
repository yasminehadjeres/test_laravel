<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactTicket extends Model
{
    public $timestamps = false;
    protected $table = "contact_ticket";
    //
}
