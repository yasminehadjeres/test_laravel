<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_ticket', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('ticket_id')->unsigned();
            $table->foreign('ticket_id')->references('id')->on('tickets')
						->onDelete('restrict')
						->onUpdate('restrict');
            $table->integer('contact_id')->unsigned();
            $table->foreign('contact_id')->references('id')->on('contacts')
						->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->boolean('was_selected')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact_ticket', function(Blueprint $table) {
			$table->dropForeign('contacts_tickets_ticket_id_foreign');
			$table->dropForeign('contacts_tickets_contact_id_foreign');
		});
        Schema::dropIfExists('contact_ticket');
    }
}
