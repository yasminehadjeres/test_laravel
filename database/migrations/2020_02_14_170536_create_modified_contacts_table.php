<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModifiedContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modified_contacts', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('name')->nullable();
            $table->string('lastname')->nullable();
            $table->string('email')->nullable();
            $table->string('title')->nullable();
            $table->string('company')->nullable();
            $table->string('country')->nullable();
            // is_processed : pour dire si le contact a été traité ou pas
            $table->boolean('is_processed');
            // is_valid : pour dire si le contact modifié à été validé par l'admin
            $table->boolean('is_valid')->default(false);
            $table->integer('ticket_id')->unsigned()->nullable();
            $table->foreign('ticket_id')->references('id')->on('tickets')
						->onDelete('restrict')
                        ->onUpdate('restrict')
                        ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modified_contacts');
    }
}
