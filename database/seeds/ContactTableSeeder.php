<?php

use App\Contact;
use App\Ticket;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // DB::table('contacts')->delete();

        DB::table('contacts')->insert([
            'name' => 'yazid',
            'lastname'=>'abdallah',
            'email' => 'yazidabdallah@gmail.com',
            'title' => 'senior IT manager',
            'company' => 'KPMG',
            'country' => 'Algérie',
            'processed' => false,
        ]);

        DB::table('contacts')->insert([
            'name' => 'yazid',
            'lastname'=>'abdallah',
            'email' => 'ayazid@kpmg.com',
            'title' => 'IT manager',
            'company' => 'kpmg',
            'country' => 'Algerie',
            'processed' => false,
        ]);
        DB::table('contacts')->insert([
            'name' => 'abdallah',
            'lastname'=>'yazid',
            'email' => 'ayazid',
            'title' => 'IT manager',
            'company' => 'kpmg',
            'country' => 'Algerie',
            'processed' => false,
        ]);
        DB::table('contacts')->insert([
            'name' => 'ahmed',
            'lastname'=>'bentaher',
            'email' => 'aben@kgpm.com',
            'title' => 'junior IT',
            'company' => 'kpmg',
            'country' => 'France',
            'processed' => false,
        ]);


        DB::table('contacts')->insert([
        'name' => 'ahmed',
        'lastname'=>'bentaher',
        'email' => 'aben@gmail.com',
        'title' => '',
        'company' => '',
        'country' => '',
        'processed' => false,
        ]);


        DB::table('contacts')->insert([
        'name' => 'kamel',
        'lastname'=>'ouali',
        'email' => 'kouali@gmail.com',
        'title' => 'Game designer',
        'company' => 'GameDz',
        'country' => 'Algeria',
        'processed' => false,
        ]);



        DB::table('contacts')->insert([
        'name' => 'radia',
        'lastname'=>'soulaf',
        'email' => 'sou@gmail.com',
        'title' => 'Data engineer',
        'company' => 'Orange',
        'country' => 'France',
        'processed' => false,
        ]);
        DB::table('contacts')->insert([
        'name' => 'salima',
        'lastname'=>'soukri',
        'email' => 'sou@gmail.com',
        'title' => 'Data scientist',
        'company' => 'facebook',
        'country' => 'France',
        'processed' => false,
        ]);



        DB::table('contacts')->insert([
        'name' => 'lotfi',
        'lastname'=>'saadi',
        'email' => 'losaadi@gmail.com',
        'title' => 'Data scientist',
        'company' => 'kpmg',
        'country' => 'France',
        'processed' => false,
        ]);


        DB::table('contacts')->insert([
        'name' => 'lotfi',
        'lastname'=>'saadi',
        'email' => 'saadi@gmail.com',
        'title' => 'Data scientist',
        'company' => 'facebook',
        'country' => 'France',
        'processed' => false,
        ]);
                
    }
}
