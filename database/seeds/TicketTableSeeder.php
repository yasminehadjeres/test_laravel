<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Ticket;

class TicketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Ticket::create(["id" => 1, "creation_date" => Carbon::now(),"is_active" => false, "waiting_for_validation" => false, "done" => false,"validation_date" => NULL]);
        Ticket::create(["id" => 2, "creation_date" => Carbon::now(),"is_active" => false, "waiting_for_validation" => false, "done" => false,"validation_date" => NULL]);
        Ticket::create(["id" => 3, "creation_date" => Carbon::now(),"is_active" => false, "waiting_for_validation" => false, "done" => false,"validation_date" => NULL]);
        Ticket::create(["id" => 4, "creation_date" => Carbon::now(),"is_active" => false, "waiting_for_validation" => false, "done" => false,"validation_date" => NULL]);


    }
}
