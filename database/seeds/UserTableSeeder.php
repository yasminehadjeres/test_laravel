<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Smith',
            'email' => 'smith@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin'=> false,
            'stars'=> 5,
        ]);
        User::create([
            'name' => 'David',
            'email' => 'david@gmail.com',
            'password' => bcrypt('secret'),
            'is_admin'=> false,
            'stars'=> 5,
        ]);
        User::create([
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => bcrypt('user'),
            'is_admin'=> false,
            'stars'=> 5,
        ]);
    }
}
