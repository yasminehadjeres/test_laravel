<?php

use Illuminate\Database\Seeder;
use App\ContactTicket;

class TicketContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            ContactTicket::create(["id" => 1, "ticket_id" => 1, "contact_id" => 1]);
            ContactTicket::create(["id" => 2, "ticket_id" => 1, "contact_id" => 2]);
            ContactTicket::create(["id" => 3, "ticket_id" => 1, "contact_id" => 3]);
            ContactTicket::create(["id" => 4, "ticket_id" => 2, "contact_id" => 4]); 
            ContactTicket::create(["id" => 5, "ticket_id" => 2, "contact_id" => 5]);
            ContactTicket::create(["id" => 6, "ticket_id" => 2, "contact_id" => 6]);
            ContactTicket::create(["id" => 7, "ticket_id" => 3, "contact_id" => 7]);
            ContactTicket::create(["id" => 8, "ticket_id" => 3, "contact_id" => 8]);
            ContactTicket::create(["id" => 9, "ticket_id" => 3, "contact_id" => 9]);
            ContactTicket::create(["id" => 10, "ticket_id" => 3, "contact_id" => 10]);    //
    }
}
