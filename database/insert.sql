-- SQLite
INSERT INTO tickets VALUES(1, CURRENT_TIMESTAMP, false, true, false, NULL);
INSERT INTO tickets VALUES(2, CURRENT_TIMESTAMP, false, true, false, NULL);
INSERT INTO tickets VALUES(3, CURRENT_TIMESTAMP, false, true, false, NULL);

INSERT INTO contact_ticket VALUES (1, 1, 1);
INSERT INTO contact_ticket VALUES (2, 1, 2);
INSERT INTO contact_ticket VALUES (3, 1, 3);

INSERT INTO contact_ticket VALUES (4, 2, 4);
INSERT INTO contact_ticket VALUES (5, 2, 5);
INSERT INTO contact_ticket VALUES (6, 2, 6);

INSERT INTO contact_ticket VALUES (7, 3, 7);
INSERT INTO contact_ticket VALUES (8, 3, 8);
INSERT INTO contact_ticket VALUES (9, 3, 9);
INSERT INTO contact_ticket VALUES (10, 3, 10);

INSERT INTO working_ons VALUES (1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, 3, 1, 8);